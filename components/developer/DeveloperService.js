const axios = require('axios').default
const Developer = require('../../model/Developer')
const config = require('../../config')
const TOKEN = `Basic ${config.tokenJira}`
const URL_GET_DEVELOPERS = 'https://lean-tech.atlassian.net/rest/api/3/users?maxResults=1000'

async function createDeveloper () {
  const projects = await getDevelopers()
  projects.map(async (item, index) => {
    const dev = await Developer.findOne({ where: { Developer_Id: item.accountId } })
    if (dev === null) {
      const developer = new Developer()
      developer.Developer_Id = item.accountId
      developer.Developer_Name = item.displayName
      await developer.save()
    }
  })
  return true
}

async function getDevelopers () {
  const response = await axios.get(URL_GET_DEVELOPERS, {
    headers: {
      Authorization: TOKEN,
      Accept: 'application/json'
    }
  })
  return Array.from(response.data)
}

module.exports = {
  createDeveloper
}
