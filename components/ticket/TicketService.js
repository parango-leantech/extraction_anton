const axios = require('axios').default
const Ticket = require('../../model/Ticket')
const Project = require('../../model/Project')
const Spring = require('../sprint/SprintService')
const config = require('../../config')
const TOKEN = `Basic ${config.tokenJira}`
const URL_GET_TICKETS = 'https://lean-tech.atlassian.net/rest/api/3/search'
const Moment = require('moment')

async function createTicket () {
  const projects = await Project.findAll()
  for (const project of projects) {
    console.log(project.Project_Name)
    const ticketList = await getTicketByProject(project.Project_Name)
    if (ticketList.length === 0) {
      continue
    }
    for (const ticketItem of ticketList) {
      await Spring.createSprint(ticketItem.fields.customfield_10020, ticketItem.id)
      if (ticketItem.fields.status === 'DONE' || Moment().diff(new Date(ticketItem.fields.updated), 'month') > 1) continue
      const findTicket = await Ticket.findOne({ where: { Ticket_Id: ticketItem.id } })
      try {
        if (findTicket !== null) {
          findTicket.Ticket_Code = ticketItem.key
          findTicket.Ticket_Name = ticketItem.fields.summary
          findTicket.Developer_Id = ticketItem.fields.assignee ? ticketItem.fields.assignee.accountId : 0
          findTicket.Project_Id = ticketItem.fields.project.id
          findTicket.Ticket_Points = ticketItem.fields.customfield_10016
          findTicket.Ticket_Type = ticketItem.fields.issuetype.name ? ticketItem.fields.issuetype.name : ''
          findTicket.Ticket_Status = ticketItem.fields.status ? ticketItem.fields.status.name : ''
          findTicket.Ticket_Created = ticketItem.fields.created ? ticketItem.fields.created : ''
          findTicket.Ticket_Updated = ticketItem.fields.updated ? ticketItem.fields.updated : ''
          await findTicket.save()
        } else {
          const ticket = new Ticket()
          ticket.Ticket_Id = ticketItem.id
          ticket.Ticket_Code = ticketItem.key
          ticket.Ticket_Name = ticketItem.fields.summary
          ticket.Developer_Id = ticketItem.fields.assignee ? ticketItem.fields.assignee.accountId : 0
          ticket.Project_Id = ticketItem.fields.project.id
          ticket.Ticket_Points = ticketItem.fields.customfield_10016
          ticket.Ticket_Type = ticketItem.fields.issuetype.name ? ticketItem.fields.issuetype.name : ''
          ticket.Ticket_Status = ticketItem.fields.status ? ticketItem.fields.status.name : ''
          ticket.Ticket_Created = ticketItem.fields.created ? ticketItem.fields.created : ''
          ticket.Ticket_Updated = ticketItem.fields.updated ? ticketItem.fields.updated : ''
          await ticket.save()
        }
      } catch (err) {
        console.log(err)
      }
    }
  }
  return true
}

async function getTicketByProject (projectName) {
  console.log('Inicio del proceso')
  let startAt = 0
  let total = 0
  let arrayTotalList = []
  const headers = {
    Authorization: TOKEN,
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
  try {
    do {
      const bodyData = {
        jql: 'project = "' + projectName + '"',
        maxResults: 100,
        startAt: startAt
      }
      const response = await axios.post(URL_GET_TICKETS, bodyData, {
        headers
      })
      arrayTotalList = arrayTotalList.concat(response.data.issues)
      total = response.data.total
      startAt = startAt + 100
    } while (startAt <= total)

    console.log(`Total de registros + ${arrayTotalList.length}`)
    return arrayTotalList
  } catch (error) {
    console.error('No information')
  } finally {
    return arrayTotalList
  }
}

module.exports = {
  createTicket
}
