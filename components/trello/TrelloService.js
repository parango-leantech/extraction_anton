const axios = require('axios').default
const Project = require('../../model/Project')
const Dev = require('../../model/Developer')
const Accounts = require('../../model/Accounts')
const { createIssueDev, createIssueProject } = require('../exceptions/ExceptionsDevelopersServices')
const { createNews } = require('../news/NewsService')
const { saveCommentAccounts, saveCommentDevelopers, saveCommentProjects } = require('./CommentsServices')
const config = require('../../config')
const TOKEN = config.tokenTrello
const KEY = config.keyTrello
const URL_BOARDS = `https://api.trello.com/1/members/parango/boards?key=${KEY}&token=${TOKEN}`
const customFieldArray = []
const informationList = []
const boardsArray = ['Raimundo Puche AM', 'Kevin Garcia AM']

async function initTrello () {
  const boards = await axios.get(URL_BOARDS)
  for (const board of boards.data) {
    if (!boardsArray.includes(board.name)) continue
    const cards = await axios.get(`https://api.trello.com/1/boards/${board.id}/cards?key=${KEY}&token=${TOKEN}`)
    for (const card of cards.data) {
      console.log(card.name)
      await createAccountsByCardId(card.id, board.name)
      const cardFiels = await axios.get(`https://api.trello.com/1/cards/${card.id}/customFieldItems?key=${KEY}&token=${TOKEN}`)
      for (const fields of cardFiels.data) {
        const nameField = await getNameCustomField(fields.idCustomField)
        // Projet
        if (nameField.name === 'Jira Id') {
          createNews(card.idChecklists, fields.value.number)
          const project = await Project.findOne({ where: { Project_Id: fields.value.number } })
          for (const fieldsJira of cardFiels.data) {
            const nameFieldJira = await getNameCustomField(fieldsJira.idCustomField)
            if (project === null) {
              continue
            }
            project.Project_Name_Trello = card.name
            if (nameFieldJira.name === 'Start date') {
              project.Project_Start_date = fieldsJira.value.date
            }
            if (nameFieldJira.name === 'Delivery date') {
              project.Project_Delivery_Date = fieldsJira.value.date
            }
            if (nameFieldJira.name === 'Account') {
              for (const account of nameFieldJira.options) {
                if (fieldsJira.idValue === account.id) {
                  const accountItem = await Accounts.findOne({ where: { Account_Name: account.value.text } })
                  if (accountItem === null) break
                  project.Account_Id = accountItem.Account_Id
                  break
                }
              }
            }
            if (nameFieldJira.name === 'Project Status') {
              for (const account of nameFieldJira.options) {
                if (account.id === fieldsJira.idValue) {
                  project.Project_Status = account.value.text
                  break
                }
              }
            }
          }
          await saveCommentProjects(card.id, project.Project_Id) // Nombre del proyecto trello
          await project.save()
          createIssueProjectItem(project)
        }
        if (nameField.name === 'Email') {
          const dev = await Dev.findOne({ where: { Developer_Email: fields.value.text } })
          if (dev === null) continue
          dev.Developer_Is_Trello = 1
          for (const fieldsDev of cardFiels.data) {
            const nameFieldDev = await getNameCustomField(fieldsDev.idCustomField)
            if (nameFieldDev.name === 'Salary') {
              dev.Developer_Salary = fieldsDev.value.number
            }
            if (nameFieldDev.name === 'Happiness') {
              dev.Developer_happiness = fieldsDev.value.number
            }
            if (nameFieldDev.name === 'Monthly Sales') {
              dev.Developer_Monthly_Sales = fieldsDev.value.number
            }

            if (nameFieldDev.name === 'Primary Technology') {
              dev.Developer_Primary_Tech = fieldsDev.value.text
            }

            if (nameFieldDev.name === 'Start date') {
              dev.Developer_Start_Date = fieldsDev.value.date
            }

            if (nameFieldDev.name === 'Seniority') {
              for (const seniority of nameFieldDev.options) {
                if (fieldsDev.idValue === seniority.id) {
                  dev.Developer_Seniority = seniority.value.text
                  break
                }
              }
            }
          }
          await dev.save()
          await saveCommentDevelopers(card.id, dev.Developer_Id)
          await createIssueDeveloper(dev)
        }
      }
    }
  }
  return true
}

async function createIssueDeveloper (dev) {
  if (dev.Developer_Salary === null) {
    await createIssueDev(dev.Developer_Id, 'The developer needs Developer Salary')
  }
  if (dev.Developer_Monthly_Sales === null) {
    await createIssueDev(dev.Developer_Id, 'The developer needs Developer Monthly Sales')
  }
  if (dev.Developer_Seniority === null) {
    await createIssueDev(dev.Developer_Id, 'The developer needs Developer Seniority')
  }
}

async function createIssueProjectItem (project) {
  if (project.Project_Start_date === null) {
    await createIssueProject(project.Project_Id, 'The project needs start date')
  }
  if (project.Project_Delivery_Date === null) {
    await createIssueProject(project.Project_Id, 'The developer needs delivery date')
  }
  if (project.Project_Status === null) {
    await createIssueProject(project.Project_Id, 'The developer needs status')
  }
}

async function getNameCustomField (id) {
  if (customFieldArray[id]) {
    return customFieldArray[id]
  }
  const fieldInfo = await axios.get(`https://api.trello.com/1/customFields/${id}?key=${KEY}&token=${TOKEN}`)
  customFieldArray[id] = fieldInfo.data
  return customFieldArray[id]
}

async function getInformationListByListId (ListId) {
  if (informationList[ListId]) {
    return informationList[ListId]
  }
  const fieldInfo = await axios.get(`https://api.trello.com/1/lists/${ListId}?key=${KEY}&token=${TOKEN}`)
  informationList[ListId] = fieldInfo.data
  return informationList[ListId]
}

async function createAccountsByCardId (cardId, managerName) {
  const cardFiels = await axios.get(`https://api.trello.com/1/cards/${cardId}?key=${KEY}&token=${TOKEN}`)
  const res = cardFiels.data
  for (const label of res.labels) {
    if ((label.name === 'Dedicated PM' || label.name === 'PM' || label.name === 'PMB' || label.name === 'Dedicated')) {
      const listInformation = await getInformationListByListId(res.idList)
      if (listInformation.name !== 'Accounts') return false
      const account = await Accounts.findOne({ where: { Account_Id: cardId } })
      if (account === null) {
        const accountNew = new Accounts()
        accountNew.Account_type = label.name
        accountNew.Account_Name = res.name
        accountNew.Account_Id = cardId
        accountNew.Account_Manager = managerName
        const cardFiels = await axios.get(`https://api.trello.com/1/cards/${cardId}/customFieldItems?key=${KEY}&token=${TOKEN}`)
        for (const fields of cardFiels.data) {
          const nameField = await getNameCustomField(fields.idCustomField)
          if (nameField.name === 'Start date') {
            accountNew.Project_Start_date = fields.value.date || ''
          }
          if (nameField.name === 'Delivery date') {
            accountNew.Project_Delivery_Date = fields.value.date || ''
          }
          if (nameField.name === 'Project Status') {
            console.log(fields.idValue, 'fields.idValue')
            console.log(nameField.options)
            for (const accountItem of nameField.options) {
              if (accountItem.id === fields.idValue) {
                accountNew.Account_Status = accountItem.value.text
              }
            }
          }
        }
        await accountNew.save()
        await saveCommentAccounts(cardId, accountNew.Account_Id)
      } else {
        await saveCommentAccounts(cardId, account.Account_Id)
      }
    }
  }
}

function sleep (ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

module.exports = {
  initTrello
}
