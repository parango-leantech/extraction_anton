const axios = require('axios').default
const CommentsProjects = require('../../model/CommentsProjects')
const CommentsAccounts = require('../../model/CommentsAccounts')
const CommentsDevelopers = require('../../model/CommentsDevelopers')
const config = require('../../config')
const TOKEN = config.tokenTrello
const KEY = config.keyTrello

async function saveCommentProjects (cardId, projectId) {
  const cardCommetsProjects = await axios.get(`https://api.trello.com/1/cards/${cardId}/actions?key=${KEY}&token=${TOKEN}`)
  for (const commentItem of cardCommetsProjects.data) {
    const commentFind = await CommentsProjects.findOne({ where: { Comment_Id: commentItem.id } })
    if (commentFind === null) {
      const comment = new CommentsProjects()
      comment.Comment_Id = commentItem.id
      comment.Project_id = projectId
      comment.Comment_Text = commentItem.data.text
      comment.Comment_Type = commentItem.type
      comment.Comment_Date = commentItem.date
      comment.Comment_Username = commentItem.username
      comment.Comment_FullName = commentItem.fullName
      await comment.save()
    } else {
      commentFind.Comment_Text = commentItem.data.text
      commentFind.Comment_Type = commentItem.type
      commentFind.Comment_Date = commentItem.date
      commentFind.Comment_Username = commentItem.memberCreator.username
      commentFind.Comment_FullName = commentItem.memberCreator.fullName
      await commentFind.save()
    }
  }
}

async function saveCommentAccounts (cardId, accountId) {
  const cardCommetsProjects = await axios.get(`https://api.trello.com/1/cards/${cardId}/actions?key=${KEY}&token=${TOKEN}`)
  for (const commentItem of cardCommetsProjects.data) {
    const commentFind = await CommentsAccounts.findOne({ where: { Comment_Id: commentItem.id } })
    if (commentFind === null) {
      const comment = new CommentsAccounts()
      comment.Comment_Id = commentItem.id
      comment.Account_id = accountId
      comment.Comment_Text = commentItem.data.text
      comment.Comment_Type = commentItem.type
      comment.Comment_Date = commentItem.date
      comment.Comment_Username = commentItem.username
      comment.Comment_FullName = commentItem.fullName
      await comment.save()
    } else {
      commentFind.Comment_Text = commentItem.data.text
      commentFind.Comment_Type = commentItem.type
      commentFind.Comment_Date = commentItem.date
      commentFind.Comment_Username = commentItem.memberCreator.username
      commentFind.Comment_FullName = commentItem.memberCreator.fullName
      await commentFind.save()
    }
  }
}

async function saveCommentDevelopers (cardId, developerId) {
  const cardCommetsProjects = await axios.get(`https://api.trello.com/1/cards/${cardId}/actions?key=${KEY}&token=${TOKEN}`)
  for (const commentItem of cardCommetsProjects.data) {
    const commentFind = await CommentsDevelopers.findOne({ where: { Comment_Id: commentItem.id } })
    if (commentFind === null) {
      const comment = new CommentsDevelopers()
      comment.Comment_Id = commentItem.id
      comment.Developer_id = developerId
      comment.Comment_Text = commentItem.data.text
      comment.Comment_Type = commentItem.type
      comment.Comment_Date = commentItem.date
      comment.Comment_Username = commentItem.username
      comment.Comment_FullName = commentItem.fullName
      await comment.save()
    } else {
      commentFind.Comment_Text = commentItem.data.text
      commentFind.Comment_Type = commentItem.type
      commentFind.Comment_Date = commentItem.date
      commentFind.Comment_Username = commentItem.memberCreator.username
      commentFind.Comment_FullName = commentItem.memberCreator.fullName
      await commentFind.save()
    }
  }
}

module.exports = {
  saveCommentProjects,
  saveCommentAccounts,
  saveCommentDevelopers
}
