const axios = require('axios').default
const News = require('../../model/News')
const config = require('../../config')
const TOKEN = config.tokenTrello
const KEY = config.keyTrello

const createNews = async (checklistIdArray, proyectId) => {
  for (const checklistId of checklistIdArray) {
    const response = await axios.get(`https://api.trello.com/1/checklists/${checklistId}?key=${KEY}&token=${TOKEN}`, {
      headers: {
        Authorization: TOKEN,
        Accept: 'application/json'
      }
    })
    const checkList = response.data
    for (const checkeListItem of checkList.checkItems) {
      const findItem = await News.findOne({ where: { News_Id: checkeListItem.id } })
      if (findItem != null) {
        findItem.News_State = checkeListItem.state
        await findItem.save()
        continue
      }
      const news = new News()
      news.News_Name = checkList.name
      news.News_Description = checkeListItem.name
      news.News_State = checkeListItem.state
      news.Project_Id = proyectId
      news.News_Id = checkeListItem.id
      await news.save()
    }
  }
}

module.exports = {
  createNews
}
