const Audit = require('../../model/Audit')
const createAudir = async (status) => {
  const audit = new Audit()
  audit.Audit_Status = status
  audit.save()
}

module.exports = {
  createAudir
}
