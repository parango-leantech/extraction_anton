const axios = require('axios').default
const Project = require('../../model/Project')
const config = require('../../config')
const TOKEN = `Basic ${config.tokenJira}`
const URL_GET_PROJECT = 'https://lean-tech.atlassian.net/rest/api/3/project/search?maxResults=1000&'

async function createProject () {
  const projects = await getProject()
  projects.map(async (item, index) => {
    const findProject = await Project.findOne({ where: { Project_Id: item.id } })
    console.log(item.name)
    if (findProject !== null) {
      return true
    }
    console.log(item.name)
    const project = new Project()
    project.Project_Id = item.id
    project.Project_Name = item.name

    await project.save()
  })
  return true
}

async function getProject () {
  let startAt = 0
  let total = 0
  let arrayTotalList = []
  do {
    const response = await axios.get(`${URL_GET_PROJECT}startAt=${startAt}`, {
      headers: {
        Authorization: TOKEN,
        Accept: 'application/json'
      }
    })
    startAt = startAt + 50
    total = response.data.total
    arrayTotalList = arrayTotalList.concat(Array.from(response.data.values))
  } while (startAt <= total)

  return arrayTotalList
}

module.exports = {
  createProject
}
