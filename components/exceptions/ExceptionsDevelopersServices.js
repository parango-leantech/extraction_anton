const ExceptionsDevelopers = require('../../model/ExceptionsDevelopers')
const ExceptionsProject = require('../../model/ExceptionsProject')

//ExceptionsDevelopers.destroy({ truncate: true })
//ExceptionsProject.destroy({ truncate: true })

const createIssueDev = async (devId, text) => {
  const findProject = await ExceptionsDevelopers.findOne({ where: { Developer_Id: devId, Exceptions_Developers_Description: text } })
  if (findProject !== null) return true
  const eDevelopers = new ExceptionsDevelopers()
  eDevelopers.Developer_Id = devId
  eDevelopers.Exceptions_Developers_Description = text
  await eDevelopers.save()
}

const createIssueProject = async (projectId, text) => {
  const findProject = await ExceptionsProject.findOne({ where: { Project_Id: projectId, Exceptions_Project_Description: text } })
  if (findProject !== null) return true
  const ePrject = new ExceptionsProject()
  ePrject.Project_Id = projectId
  ePrject.Exceptions_Project_Description = text
  await ePrject.save()
}

module.exports = {
  createIssueDev,
  createIssueProject
}
