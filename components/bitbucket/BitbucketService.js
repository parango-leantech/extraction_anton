const config = require('../../config')
const axios = require('axios').default
const Commit = require('../../model/Commits')

//const USERNAME_BITBUCKET = config.usernameBitbucket
//const PASSWORD_BITBUCKET = config.passwordBitbucket
// const token = Buffer.from(`${USERNAME_BITBUCKET}:${PASSWORD_BITBUCKET}`, 'base64').toString()
const URL_BITBUCKET_REPOSITORIES = 'https://api.bitbucket.org/2.0/user/permissions/repositories'

const headers = {
  Authorization: `Basic cGFyYW5nby1sZWFudGVjaDpQQGJsbzEwMzU=`
}

const createCommits = async () => {
  try {
    const repositories = await getRepositoies()
    for (const repo of repositories) {
      const urlCommits = `https://api.bitbucket.org/2.0/repositories/${repo.repository.full_name}/commits`
      const commits = await getCommits(urlCommits)
      for (const commitItem of commits) {
        const commit = new Commit()
        commit.Developer_Id = commitItem.author.user ? commitItem.author.user.account_id : ''
        commit.Commit_Message = commitItem.message || 'Commit'
        commit.Commit_Hash = commitItem.hash
        commit.Commit_Type = commitItem.type
        commit.Commit_Date = commitItem.date
        commit.Commit_Repository_Full_Name = commitItem.repository.full_name
        commit.Commit_Repository_Name = commitItem.repository.name
        await commit.save()
        await setTimeout(function () { return true }, 2000)
      }
    }
  } catch (error) {
    console.log(error)
  }
}

async function getRepositoies () {
  let arrayRepositories = []
  let page = 1
  let done = true
  do {
    await setTimeout(function () { return true }, 2000)
    const response = await axios.get(URL_BITBUCKET_REPOSITORIES + `/?page=${page}`, { headers })
    const repositories = response.data.values
    if (repositories.length > 0) {
      arrayRepositories = arrayRepositories.concat(repositories)
    } else {
      done = false
    }
    page++
  } while (done)

  return arrayRepositories
}

async function getCommits (url) {
  let arrayCommits = []
  let page = 1
  let done = true
  do {
    await setTimeout(function () { return true }, 2000)
    const response = await axios.get(url + `/?page=${page}`, { headers })
    const commits = response.data.values
    if (commits.length > 0) {
      arrayCommits = arrayCommits.concat(commits)
    } else {
      done = false
    }
    page++
  } while (done)

  return arrayCommits
}

module.exports = {
  createCommits
}
