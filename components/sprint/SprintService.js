const Sprint = require('../../model/Sprint')

async function createSprint (arraySprints, ticketId) {
  if (typeof arraySprints === 'undefined' || arraySprints === '' || arraySprints == null) {
    await Sprint.destroy({
      where: { Ticket_Id: ticketId }
    })
    return
  }
  await Sprint.destroy({
    where: { Ticket_Id: ticketId }
  })
  arraySprints.map(async item => {
    try {
      const objSprint = item

      const findSprint = await Sprint.findOne({ where: { Sprint_Id: objSprint.id, Ticket_Id: ticketId } })
      if (findSprint !== null) {
        findSprint.Sprint_Complete = objSprint.completeDate === '' ? null : objSprint.completeDate
        findSprint.Sprint_End = objSprint.endDate === '' ? null : objSprint.endDate
        findSprint.Sprint_Goal = objSprint.goal === '' ? null : objSprint.goal
        findSprint.Sprint_Name = objSprint.name === '' ? null : objSprint.name
        findSprint.Sprint_Start = objSprint.startDate === '' ? null : objSprint.startDate
        findSprint.Sprint_State = objSprint.state === '' ? null : objSprint.state
        await findSprint.save()
      } else {
        const sprint = new Sprint()
        sprint.Sprint_Id = objSprint.id === '' ? null : objSprint.id
        sprint.Ticket_Id = ticketId === '' ? null : ticketId
        sprint.Sprint_Complete = objSprint.completeDate === '' ? null : objSprint.completeDate
        sprint.Sprint_End = objSprint.endDate === '' ? null : objSprint.endDate
        sprint.Sprint_Goal = objSprint.goal === '' ? null : objSprint.goal
        sprint.Sprint_Name = objSprint.name === '' ? null : objSprint.name
        sprint.Sprint_Start = objSprint.startDate === '' ? null : objSprint.startDate
        sprint.Sprint_State = objSprint.state === '' ? null : objSprint.state
        await sprint.save()
      }
    } catch (error) {
      console.log('There is no changes in the update, lets continue the progress...')
    }
  })
}

function formaterString (tramaString) {
  const fistBrackets = tramaString.indexOf('[')
  const endBrackets = tramaString.indexOf(']')
  const cleanTramaString = tramaString.substring(fistBrackets + 1, endBrackets)
  var objetcSprint = {}

  cleanTramaString.split(',').map(item => {
    const atr = item.split('=')
    objetcSprint[atr[0]] = atr[1]
  })
  return objetcSprint
}

module.exports = {
  createSprint,
  formaterString
}
