const dotenv = require('dotenv')
dotenv.config()

module.exports = {
  tokenJira: process.env.TOKEN_JIRA || '',
  tokenTrello: process.env.TOKEN_TRELLO || '',
  keyTrello: process.env.KEY_TRELLO || '',
  dbName: process.env.DB_NAME || '',
  dbUser: process.env.DB_USER || '',
  dbPassword: process.env.DB_PASSWORD || '',
  dbHost: process.env.DB_HOST || '',
  usernameBitbucket: process.env.USERNAME_BITBUCKET || '',
  passwordBitbucket: process.env.PASSWORD_BITBUCKET || ''
}
