const db = require('../config/db')
const sequelize = db.sequelize
const Sequelize = db.Sequelize

const Model = Sequelize.Model
class Audit extends Model {}
Audit.init({
  // attributes
  id: {
    type: Sequelize.BIGINT(11),
    primaryKey: true,
    autoIncrement: true,
    allowNull: false
  },
  date: {
    type: Sequelize.DATE
  },
  Audit_Status: {
    type: Sequelize.STRING
  }
}, {
  sequelize,
  modelName: 'audits',
  timestamps: false
})

module.exports = Audit
