const db = require('../config/db')
const sequelize = db.sequelize
const Sequelize = db.Sequelize

const Model = Sequelize.Model
class News extends Model {}
News.init({
  // attributes
  id: {
    type: Sequelize.BIGINT(11),
    primaryKey: true,
    autoIncrement: true,
    allowNull: false
  },
  News_Name: {
    type: Sequelize.STRING
  },
  News_Description: {
    type: Sequelize.STRING
  },
  News_State: {
    type: Sequelize.STRING
  },
  Project_Id: {
    type: Sequelize.BIGINT(11),
    allowNull: false
  },
  News_Id: {
    type: Sequelize.STRING
  }
}, {
  sequelize,
  modelName: 'dim_news',
  timestamps: false
})

module.exports = News
