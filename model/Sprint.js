const db = require('../config/db')
const sequelize = db.sequelize
const Sequelize = db.Sequelize

const Model = Sequelize.Model
class Sprint extends Model {}
Sprint.init({
  // attributes
  id: {
    type: Sequelize.BIGINT(11),
    primaryKey: true,
    autoIncrement: true,
    allowNull: false
  },
  Sprint_Id: {
    type: Sequelize.BIGINT(11),
    allowNull: false
  },
  Ticket_Id: {
    type: Sequelize.BIGINT(11),
    allowNull: false
  },
  Sprint_Complete: {
    type: Sequelize.DATE
  },
  Sprint_End: {
    type: Sequelize.DATE
  },
  Sprint_Goal: {
    type: Sequelize.STRING
  },
  Sprint_Name: {
    type: Sequelize.STRING
  },
  Sprint_Start: {
    type: Sequelize.DATE
  },
  Sprint_State: {
    type: Sequelize.STRING
  }
}, {
  sequelize,
  modelName: 'dim_sprints',
  timestamps: false
})

module.exports = Sprint
