const db = require('../config/db')
const sequelize = db.sequelize
const Sequelize = db.Sequelize

const Model = Sequelize.Model
class Ticket extends Model {}
Ticket.init({
  // attributes
  id: {
    type: Sequelize.BIGINT(11),
    primaryKey: true,
    autoIncrement: true,
    allowNull: false
  },
  Ticket_Id: {
    type: Sequelize.BIGINT(11),
    allowNull: false
  },
  Ticket_Code: {
    type: Sequelize.STRING,
    allowNull: false
  },
  Ticket_Name: {
    type: Sequelize.STRING
  },
  Ticket_Type: {
    type: Sequelize.STRING
  },
  Project_Id: {
    type: Sequelize.BIGINT(11),
    allowNull: false
  },
  Developer_Id: {
    type: Sequelize.STRING
  },
  Ticket_Points: {
    type: Sequelize.INTEGER
  },
  Ticket_Status: {
    type: Sequelize.STRING
  },
  Ticket_Created: {
    type: Sequelize.DATE
  },
  Ticket_Updated: {
    type: Sequelize.DATE
  }
}, {
  sequelize,
  modelName: 'fact_tickets',
  timestamps: false
})

module.exports = Ticket
