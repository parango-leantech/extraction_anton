const db = require('../config/db')
const sequelize = db.sequelize
const Sequelize = db.Sequelize

const Model = Sequelize.Model
class Commit extends Model {}
Commit.init({
  // attributes
  id: {
    type: Sequelize.BIGINT(11),
    primaryKey: true,
    autoIncrement: true,
    allowNull: false
  },
  Commit_Hash: {
    type: Sequelize.STRING,
    allowNull: false
  },
  Developer_Id: {
    type: Sequelize.STRING
  },
  Commit_Message: {
    type: Sequelize.STRING,
    allowNull: false
  },
  Commit_Type: {
    type: Sequelize.STRING
  },
  Commit_Repository_Full_Name: {
    type: Sequelize.STRING
  },
  Commit_Repository_Name: {
    type: Sequelize.STRING
  },
  Commit_Date: {
    type: Sequelize.DATE
  }
}, {
  sequelize,
  modelName: 'dim_Commits',
  timestamps: false
})

module.exports = Commit
