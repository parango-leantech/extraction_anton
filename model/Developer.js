const db = require('../config/db')
const sequelize = db.sequelize
const Sequelize = db.Sequelize

const Model = Sequelize.Model
class Developer extends Model {}
Developer.init({
  // attributes
  id: {
    type: Sequelize.BIGINT(11),
    primaryKey: true,
    autoIncrement: true,
    allowNull: false
  },
  Developer_Id: {
    type: Sequelize.STRING,
    allowNull: false
  },
  Developer_Name: {
    type: Sequelize.STRING,
    allowNull: false
  },
  Developer_Email: {
    type: Sequelize.STRING
  },
  Developer_Salary: {
    type: Sequelize.BIGINT(11)
  },
  Developer_Monthly_Sales: {
    type: Sequelize.BIGINT(11)
  },
  Developer_happiness: {
    type: Sequelize.BIGINT(11)
  },
  Developer_Seniority: {
    type: Sequelize.STRING
  },
  Developer_Is_Trello: {
    type: Sequelize.STRING
  },
  Developer_Primary_Tech: {
    type: Sequelize.STRING
  },
  Developer_Start_Date: {
    type: Sequelize.DATE
  }
}, {
  sequelize,
  modelName: 'dim_developers',
  timestamps: false
})

module.exports = Developer
