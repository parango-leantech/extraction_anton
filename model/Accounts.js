const db = require('../config/db')
const sequelize = db.sequelize
const Sequelize = db.Sequelize

const Model = Sequelize.Model
class Account extends Model {}
Account.init({
  // attributes
  id: {
    type: Sequelize.BIGINT(11),
    primaryKey: true,
    autoIncrement: true,
    allowNull: false
  },
  Account_Id: {
    type: Sequelize.STRING,
    allowNull: false
  },
  Account_Name: {
    type: Sequelize.STRING,
    allowNull: false
  },
  Account_type: {
    type: Sequelize.STRING
  },
  Account_Start_date: {
    type: Sequelize.DATE
  },
  Account_Delivery_Date: {
    type: Sequelize.DATE
  },
  Account_Status: {
    type: Sequelize.STRING
  },
  Account_Manager: {
    type: Sequelize.STRING
  }
}, {
  sequelize,
  modelName: 'dim_accounts',
  timestamps: false
})

module.exports = Account
