const db = require('../config/db')
const sequelize = db.sequelize
const Sequelize = db.Sequelize

const Model = Sequelize.Model
class Project extends Model {}
Project.init({
  // attributes
  id: {
    type: Sequelize.BIGINT(11),
    primaryKey: true,
    autoIncrement: true,
    allowNull: false
  },
  Project_Id: {
    type: Sequelize.BIGINT(11),
    allowNull: false
  },
  Project_Name: {
    type: Sequelize.STRING,
    allowNull: false
  },
  Project_Name_Trello: {
    type: Sequelize.STRING
  },
  Account_Id: {
    type: Sequelize.STRING
  },
  Project_Start_date: {
    type: Sequelize.DATE
  },
  Project_Delivery_Date: {
    type: Sequelize.DATE
  },
  Project_Status: {
    type: Sequelize.STRING
  }
}, {
  sequelize,
  modelName: 'dim_projects',
  timestamps: false
})

module.exports = Project
