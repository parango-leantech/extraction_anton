const db = require('../config/db')
const sequelize = db.sequelize
const Sequelize = db.Sequelize

const Model = Sequelize.Model
class ExceptionsDevelopers extends Model {}
ExceptionsDevelopers.init({
  // attributes
  id: {
    type: Sequelize.BIGINT(11),
    primaryKey: true,
    autoIncrement: true,
    allowNull: false
  },
  Developer_Id: {
    type: Sequelize.STRING,
    allowNull: false
  },
  Exceptions_Developers_Description: {
    type: Sequelize.STRING
  },
  Exceptions_Developers_creation: {
    type: Sequelize.DATE
  }
}, {
  sequelize,
  modelName: 'dim_exceptions_developers',
  timestamps: false
})

module.exports = ExceptionsDevelopers
