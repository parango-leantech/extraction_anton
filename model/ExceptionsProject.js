const db = require('../config/db')
const sequelize = db.sequelize
const Sequelize = db.Sequelize

const Model = Sequelize.Model
class ExceptionsProject extends Model {}
ExceptionsProject.init({
  // attributes
  id: {
    type: Sequelize.BIGINT(11),
    primaryKey: true,
    autoIncrement: true,
    allowNull: false
  },
  Project_Id: {
    type: Sequelize.STRING,
    allowNull: false
  },
  Exceptions_Project_Description: {
    type: Sequelize.STRING
  },
  Exceptions_Project_creation: {
    type: Sequelize.DATE
  }
}, {
  sequelize,
  modelName: 'dim_exceptions_projects',
  timestamps: false
})

module.exports = ExceptionsProject
