const db = require('../config/db')
const sequelize = db.sequelize
const Sequelize = db.Sequelize

const Model = Sequelize.Model
class CommentDeveloper extends Model {}
CommentDeveloper.init({
  // attributes
  id: {
    type: Sequelize.BIGINT(11),
    primaryKey: true,
    autoIncrement: true,
    allowNull: false
  },
  Comment_Id: {
    type: Sequelize.STRING,
    allowNull: false
  },
  Developer_id: {
    type: Sequelize.STRING
  },
  Comment_Text: {
    type: Sequelize.STRING
  },
  Comment_Type: {
    type: Sequelize.STRING
  },
  Comment_Date: {
    type: Sequelize.DATE
  },
  Comment_Username: {
    type: Sequelize.STRING
  },
  Comment_FullName: {
    type: Sequelize.STRING
  }
}, {
  sequelize,
  modelName: 'dim_developers_comments',
  timestamps: false
})

module.exports = CommentDeveloper
