const Developer = require('./components/developer/DeveloperService')
const Project = require('./components/project/ProjectService')
const Ticket = require('./components/ticket/TicketService')
const Trello = require('./components/trello/TrelloService')
const Bitbucket = require('./components/bitbucket/BitbucketService')
const Audit = require('./components/audit/AuditService')
const CronJob = require('cron').CronJob

async function main () {
  try {
    console.log('Devs')
    await Developer.createDeveloper()

    console.log('Project')
    await Project.createProject()

    console.log('Tickets')
    await Ticket.createTicket()

    console.log('Trello')
    await Trello.initTrello()

    //console.log('Bitbucket')
    //await Bitbucket.createCommits()

    await Audit.createAudir('Done')
  } catch (error) {
    //await Audit.createAudir('Error')
    console.error(error)
  }
}

//main()

var job = new CronJob(
  '0 23 * * *',
  main,
  null,
  true,
  'America/Los_Angeles'
)
console.log('******************** Job Active ********************')
job.start()
